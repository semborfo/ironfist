from django.urls import path

from main import views

urlpatterns = [
    path('', views.make_table, name='make_table'),
]