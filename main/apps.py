from django.apps import AppConfig


class IronfistAppConfig(AppConfig):
    name = 'main'
