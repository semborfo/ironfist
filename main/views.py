from django.shortcuts import render
from ancillary_stuff.utils import *
from staffing.models import Staffing
from ancillary_stuff.objects import *
from replacement.models import Replacement
from employers.models import Employer
import copy
from ancillary_stuff.const import *

def make_table(request):
    form = request.POST
    daily_schedule = copy.deepcopy(Staffing.objects.all())


    if request.method == "POST":
        date = form["date"]
        splitted_date = date.split(' - ')
        start = datetime.strptime(splitted_date[0], "%m/%d/%Y")
        end = datetime.strptime(splitted_date[1], "%m/%d/%Y")
    else:
        start = datetime.today().date() + timedelta(days=-(datetime.today().weekday()))
        end = start + timedelta(6)

    date_range = make_date_range(end, start)
    TR_date_range = [TR_data(date) for date in date_range]
    shifts_placed_by_weekday = make_shifts_by_weekday(daily_schedule)
    shifts_by_date = {}
    for date in TR_date_range:
        shifts_by_date[date] = list(
            WorkShift(date.weekday, item, date) for item in
            sorted(shifts_placed_by_weekday[date.weekday], key=operator.attrgetter('start_time')))

    update_persons_with_all_shifts(shifts_by_date)

    min_time = find_min_time(shifts_by_date.values())
    max_time = find_max_time(shifts_by_date.values())

    time_range = (max_time - min_time).seconds // 3600
    TimeRow_time_range = [TimeRow(min_time, i) for i in range(0, time_range + 1)]
    TimeRow_time_range_by_hour = {i.hour: i for i in TimeRow_time_range}
    for date in shifts_by_date:
        replace(replacement=Replacement, date=date, shifts=shifts_by_date[date])
        EmptyShifts = make_empty_shifts(shifts_by_date, TimeRow_time_range, date)
        for person in shifts_by_date[date]:
            TimeRow_time_range_by_hour[person.start_hour].append_shift(person)
        for emp_shift in EmptyShifts:
            TimeRow_time_range_by_hour[emp_shift.start_hour].append_shift(emp_shift)


        colspan = find_colspan(shifts_by_date[date])
        date.set_colspan(colspan)


    return render(request, 'main/main.html',
                  context={'dates': sorted(TR_date_range, key=operator.attrgetter('date')),
                           'timerows': TimeRow_time_range}, )

def replace(replacement, date, shifts):
    replacement_in_date = replacement.objects.filter(date=date.date)
    if replacement_in_date:
        for shift in shifts:
            for replace in replacement_in_date:
                if replace.name == shift.name:
                    shift.set_need_replacement()
                    shift.set_replacer(replace.taker)
        shifts_to_delete = set()
        for shift in shifts:
            if shift in shifts_to_delete:
                continue
            for other_shift in shifts:
                if (shift.name.name == other_shift.name.name or shift.name.name == other_shift.replacer) and shift != other_shift:
                    start_time = min(shift.start_time, other_shift.start_time) #TODO
                    end_time = max(shift.end_time, other_shift.end_time) #TODO
                    shift.set_start_time(start_time)
                    shift.set_end_time(end_time)
                    shifts_to_delete.add(other_shift)
        for shift in shifts_to_delete:
            shifts.remove(shift)
    else:
        return False