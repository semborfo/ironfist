from django.shortcuts import render
from ancillary_stuff.utils import *
from .models import Replacement
from staffing.models import Staffing
from employers.models import Employer
from ancillary_stuff.const import *
from copy import deepcopy

DELETE_REPLACEMENT = 'DeleteReplacement'
PICK_REPLACER = 'PickReplacer'
PUT_REPLACER = 'PutReplacer'


def show_replacement(request):
    if request.method == "POST":
        choose_an_action(request.POST)
    names = [person.name for person in Employer.objects.all()]
    return render(request, 'replacement/replacement.html',
                  context={'replacements': Replacement.objects.all(), 'names': names})


def choose_an_action(RequestPost):
    action_type = RequestPost.get('type')
    if action_type == DELETE_REPLACEMENT:
        delete_replacement(RequestPost)
    elif action_type == PUT_REPLACER:
        put_replacemer(RequestPost)
    elif action_type == PICK_REPLACER:
        pick_replacer(RequestPost)
    elif not action_type:
        make_replacement(RequestPost)


def make_replacement(RequestPost):
    replacements = Replacement.objects.all()
    name = RequestPost["name"]

    date = RequestPost["date"]
    splitted_date = date.split(' - ')
    start = datetime.strptime(splitted_date[0], "%m/%d/%Y")
    end = datetime.strptime(splitted_date[1], "%m/%d/%Y")
    date_range = make_date_range(end, start)

    worker = Employer.objects.get(name=name)

    shifts = Staffing.objects.filter(name=worker)

    if shifts:
        for date in date_range:
            if date >= datetime.today() and is_replacement_not_exists(replacements, date, name):
                shift_inst = shifts.filter(dayOfTheWeek=weekday_by_index[date.weekday()])
                if shift_inst:
                    if len(shift_inst) != 1:
                        print('error shift inst is more then 1')
                    else:
                        inst = deepcopy(shift_inst[0])
                        element = Replacement(name=inst.name,
                                              date=date,
                                              dayOfTheWeek=inst.dayOfTheWeek,
                                              start_time=inst.start_time,
                                              end_time=inst.end_time,
                                              )
                        element.save()


def delete_replacement(RequestPost):
    id = RequestPost['id']
    query = Replacement.objects.get(id=id)
    query.delete()

def pick_replacer(RequestPost):
    id = RequestPost['id']
    query = Replacement.objects.get(id=id)
    query.clean_taker()
    query.save()


def put_replacemer(RequestPost):
    id = RequestPost['id']
    taker = RequestPost['taker']

    query = Replacement.objects.get(id=id)
    query.taker = taker

    query.save()


def is_replacement_not_exists(replacements, date, name):
    replcements_in_curr_date = replacements.filter(date=date)
    for repl in replcements_in_curr_date:
        if repl.name.name == name:
            return False
    return True
