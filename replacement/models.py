from django.db import models
from ancillary_stuff.const import DAY_OF_THE_WEEK
from employers.models import Employer


class Replacement(models.Model):
    name = models.ForeignKey(Employer, on_delete=models.CASCADE)
    date = models.DateField(null=True)
    dayOfTheWeek = models.CharField(max_length=9, choices=DAY_OF_THE_WEEK)
    start_time = models.TimeField(null=True, max_length=123)
    end_time = models.TimeField(null=True, max_length=123)
    taker = models.CharField(max_length=30, blank=True)
    objects = models.Manager()

    def __str__(self):
        return "дата: %s,%s, %s, начало смены: %s; конец смены: %s; берет: %s" % (self.date,
                                                                       self.dayOfTheWeek, self.name, self.start_time,
                                                                       self.end_time, self.taker)

    class Meta:
        verbose_name = 'Замена'
        verbose_name_plural = 'Замены'


    def clean_taker(self):
        self.taker=''