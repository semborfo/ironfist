from django.urls import path

from replacement import views


# Create your tests here.
urlpatterns = [
    path('', views.show_replacement, name='show_replacement'),
]