$(document).ready(function(){
 $('.dates').daterangepicker();
});

$(document).ready(function(){
    $('#add_staff_start_time').datetimepicker({
        format: 'LT'
    });
});

$(function () {
    $('#add_staff_end_time').datetimepicker({
        format: 'LT'
    });
});


function DeleteReplacement(id) {
     $.ajax({
        url: "",
        type: "POST",
        dataType: "json",
        data: { csrfmiddlewaretoken: getCookie('csrftoken'), "id": id, "type": "DeleteReplacement"},
    });
    window.location.reload()
}


function PickReplacer(id) {

     $.ajax({
        url: "",
        type: "POST",
        dataType: "json",
        data: { csrfmiddlewaretoken: getCookie('csrftoken'), "id": id, "type": "PickReplacer"},
    });
    window.location.reload()
}
function PutReplacer(id) {
     var taker_list = document.getElementById("taker_"+id);
     var taker = taker_list.options[taker_list.selectedIndex].text
     console.log(taker)
     $.ajax({
        url: "",
        type: "POST",
        dataType: "json",
        data: { csrfmiddlewaretoken: getCookie('csrftoken'), "id": id, "taker": taker, "type": "PutReplacer"},
    });
    window.location.reload()
}









function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function() {
    var table = $('#schedule').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );

    table.buttons().container()
        .appendTo( '#schedule_wrapper .col-md-6:eq(0)' );
} );