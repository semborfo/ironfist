from django.db import models
from ancillary_stuff.const import GENDER_CHOICES


class Employer(models.Model):
    name = models.CharField(max_length=30, blank=True)
    phone_number = models.CharField(max_length=15, blank=True)
    email = models.EmailField(blank=True)
    sex = models.CharField(max_length=1, choices=GENDER_CHOICES,  blank=True)
    objects = models.Manager()


    def __str__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = 'Инструктор'
        verbose_name_plural = 'Инструкторы'