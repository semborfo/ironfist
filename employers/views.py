from django.shortcuts import render
from .models import Employer
from copy import deepcopy
import operator


def show_employers(request):
    employers = deepcopy(Employer.objects.all())
    return render(request, 'employers/employers.html',
                  context={'employers': sorted(employers, key=operator.attrgetter('name'))})
