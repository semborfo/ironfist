from django.urls import path

from employers import views

urlpatterns = [
    path('', views.show_employers, name='show_employers'),
]