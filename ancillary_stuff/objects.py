from datetime import timedelta, time, datetime
from ancillary_stuff.const import *
import copy


class TR_data():
    def __init__(self, date):
        self.__date = date
        self.__str_date = date.strftime("%d.%m.%Y")
        self.__colspan = 1

    @property
    def colspan(self):
        return self.__colspan

    @property
    def date(self):
        return self.__date

    @property
    def str_date(self):
        return self.__str_date

    def set_colspan(self, value):
        self.__colspan = value

    @property
    def weekday(self):
        return weekday_by_index[self.__date.weekday()]

    @property
    def class_name(self):
        return 'table-primary'


class TR_weekday():
    def __init__(self, weekday):
        self.__data = weekday
        self.__colspan = 1

    def set_colspan(self, value):
        self.__colspan = value

    @property
    def data(self):
        return self.__data

    @property
    def colspan(self):
        return self.__colspan


class WorkShift():
    def __init__(self, weekday, item, date):
        if 'builtin_function_or_method' in str(type(date.date)):
            date = date.date()
        else:
            date = date.date

        if item.end_time < item.start_time:
            self.__end_time = datetime.combine(date, item.end_time) + timedelta(days=1)
        else:
            self.__end_time = datetime.combine(date, item.end_time)
        self.__start_time = datetime.combine(date, item.start_time)

        self.__weekday = weekday
        self.__name = item.name

        self.__all_shifts_on_day = set()

        self.__colspan = 1

        self.__start_hour = item.start_time.hour
        self.__end_hour = item.end_time.hour

        self.__placed = False
        self.__decr_rowspan_flag = False

        self.__need_replacement = False
        self.__replacer = None

    @property
    def class_name(self):
        if self.need_replacement and not self.replacer:
            return 'table-danger'
        else:
            return 'table-primary'

    def set_all_shifts_on_day(self, all_shifts_on_day):
        self.__all_shifts_on_day = all_shifts_on_day

    @property
    def all_shifts_on_day(self):
        return self.__all_shifts_on_day

    @property
    def weekday(self):
        return self.__weekday

    @property
    def name(self):
        return self.__name

    @property
    def start_time(self):
        return self.__start_time

    @property
    def end_time(self):
        return self.__end_time

    @property
    def data(self):
        if self.need_replacement:
            return f'{self.replacer} {self.start_time.strftime("%H:%M")}-{self.end_time.strftime("%H:%M")}'
        else:
            return f'{self.name} {self.start_time.strftime("%H:%M")}-{self.end_time.strftime("%H:%M")}'

    @property
    def rowspan(self):
        return len(self.work_diap)

    @property
    def colspan(self):
        return self.__colspan

    @property
    def start_hour(self):
        return int(self.__start_hour)

    @property
    def end_hour(self):
        return self.__end_hour

    @property
    def work_diap(self):
        end_time = copy.deepcopy(self.__end_time.replace(minute=0))
        start_time = copy.deepcopy(self.__start_time.replace(minute=0))
        time_range = int((end_time - start_time).seconds // 3600)
        for shift in self.all_shifts_on_day:
            if shift.start_hour == self.end_hour:
                self.__decr_rowspan_flag = True
                return [(self.__start_time + timedelta(hours=i)).hour for i in range(0, time_range)]
        return [(self.__start_time + timedelta(hours=i)).hour for i in range(0, time_range + 1)]

    @property
    def is_placed(self):
        return self.__placed

    def place(self):
        self.__placed = True

    @property
    def replacer(self):
        return self.__replacer

    def set_replacer(self, replacer):
        self.__replacer = replacer

    @property
    def need_replacement(self):
        return self.__need_replacement

    def set_need_replacement(self):
        self.__need_replacement = True

    def set_start_time(self, start_time):
        self.__start_time = start_time

    def set_end_time(self, end_time):
        self.__end_time = end_time


class EmptyShift():

    def __init__(self, time_list, weekday):
        self.__data = ''
        self.__colspan = 1
        self.__rowspan = len(time_list)
        self.__start_hour = time_list[0]
        self.__end_hour = time_list[-1]
        self.__weekday = weekday
        self.__end_time = datetime.combine(datetime.today().date(), time(hour=self.__end_hour))
        self.__start_time = datetime.combine(datetime.today().date(), time(hour=self.__start_hour))
        if self.__start_hour > self.__end_hour or self.__end_hour == 0:
            self.__end_time = self.__end_time + timedelta(days=1)

        if self.__start_hour == 0:
            self.__start_time = self.__start_time + timedelta(days=1)

    @property
    def class_name(self):
        return 'table-secondary'

    @property
    def start_time(self):
        return self.__start_time

    @property
    def end_time(self):
        return self.__end_time

    @property
    def data(self):
        return self.__data

    @property
    def rowspan(self):
        return self.__rowspan

    @property
    def colspan(self):
        return self.__colspan

    @property
    def start_hour(self):
        return int(self.__start_hour)

    @property
    def end_hour(self):
        return self.__end_hour

class TimeRow():
    def __init__(self, time, i):
        self.__time = (time + timedelta(hours=i)).strftime("%H:%M")
        self.__hour = (time + timedelta(hours=i)).hour
        self.__shifts = list()

    @property
    def time(self):
        return self.__time

    @property
    def class_name(self):
        return 'table-dark'

    @property
    def hour(self):
        return self.__hour

    def append_shift(self, shift):
        self.__shifts.append(shift)

    @property
    def shifts(self):
        return self.__shifts

    @property
    def no_need_data(self):
        return False
