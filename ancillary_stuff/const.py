weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
weekday_by_index = dict(zip(range(len(weekdays)), weekdays))
index_by_weekday = dict(zip(weekdays, range(len(weekdays))))


GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

DAY_OF_THE_WEEK = (
    ('Monday', u'Monday'),
    ('Tuesday', u'Tuesday'),
    ('Wednesday', u'Wednesday'),
    ('Thursday', u'Thursday'),
    ('Friday', u'Friday'),
    ('Saturday', u'Saturday'),
    ('Sunday', u'Sunday'),
)