from ancillary_stuff.objects import EmptyShift
import operator
from datetime import datetime, timedelta

def make_empty_shifts(shifts_by_date, TimeRow_time_range, weekday):
    empty_hours = list()
    whole_workday_diap = [time.hour for time in TimeRow_time_range]
    while is_all_placed(shifts_by_date[weekday]) > 0:
        time_list_before_search=[*whole_workday_diap]
        for person in shifts_by_date[weekday]:
            if person.is_placed:
                continue
            if intersection_check(whole_workday_diap, person.work_diap):
                remove_sequence(whole_workday_diap, person.work_diap)
                person.place()
        if whole_workday_diap == time_list_before_search:
            empty_hours.append(whole_workday_diap)
            whole_workday_diap = [time.hour for time in TimeRow_time_range]
    empty_hours.append(whole_workday_diap)

    empty_hours_in_order = search_arithm_prog_in_collections(empty_hours)
    return sorted([EmptyShift(diap, weekday) for diap in empty_hours_in_order], key=operator.attrgetter('start_time'))


def is_all_placed(persons):
    for person in persons:
        if not person.is_placed:
            return True
    return False


def intersection_check(ethalon, target):
    for item in target:
        if item not in ethalon:
            return False
    return True


def search_arithm_prog_in_collections(time_diap_list):
    result = list()
    for i in time_diap_list:
        find_it = search_arithm_prog(i)
        result = [*result, *find_it]
    return result


def search_arithm_prog(time_list):
    result = list()
    while len(time_list) > 0:
        start = time_list[0]
        order_time_list = [start]
        for i in time_list:
            if i == start:
                continue
            if i - order_time_list[-1] == 1:
                order_time_list.append(i)
            else:
                break
        result.append(order_time_list)
        remove_sequence(time_list, order_time_list)
    return result


def remove_sequence(time_list, shift_list):
    for i in shift_list:
        time_list.remove(i)


def find_colspan(persons):
    whole_workhours = list()
    for person in persons:
        whole_workhours = [*whole_workhours, *person.work_diap]
    workhours_by_count = dict()
    for i in whole_workhours:
        if i not in workhours_by_count:
            workhours_by_count[i] = whole_workhours.count(i)
    return max(zip(workhours_by_count.values(), workhours_by_count.keys()))[0]


def make_shifts_by_weekday(daily_schedule):
    weekday_with_persons = dict()
    for item in daily_schedule:
        if item.dayOfTheWeek not in weekday_with_persons:
            weekday_with_persons[item.dayOfTheWeek] = set()
        weekday_with_persons[item.dayOfTheWeek].add(item)
    return weekday_with_persons


def update_persons_with_all_shifts(shifts_by_date):
    for date, persons in shifts_by_date.items():
        for person in persons:
            person.set_all_shifts_on_day(shifts_by_date[date])

def find_min_time(shift_collection):
    shifts = []
    for shift_item in shift_collection:
        shifts = [*shifts, *shift_item]
    times = []
    for shift in shifts:
        element = datetime.combine(datetime.today().date(), shift.start_time.time())
        times.append(element)
    min_time = min(set(map(lambda x: x, times)))
    min_time = min_time.replace(minute=0)
    return  min_time

def find_max_time(shift_collection):
    shifts = []
    for shift_item in shift_collection:
        shifts = [*shifts, *shift_item]
    times = []
    for shift in shifts:
        if shift.start_time.time() > shift.end_time.time():
            element = datetime.combine(datetime.today().date(), shift.end_time.time()) + timedelta(days=1)
        else:
            element = datetime.combine(datetime.today().date(), shift.end_time.time())
        times.append(element)
    max_time = max(set(map(lambda x: x, times)))
    if max_time.minute != 0:
        max_time = max_time.replace(minute=0) + timedelta(hours=1)
    return  max_time

def make_date_range(end, start):
    return [start + timedelta(days=x) for x in range(0, (end - start).days + 1)]