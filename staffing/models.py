from django.db import models
from employers.models import Employer
from time import time
from django.contrib import messages
from ancillary_stuff.const import DAY_OF_THE_WEEK



class Staffing(models.Model):
    name = models.ForeignKey(Employer, on_delete=models.CASCADE)
    dayOfTheWeek = models.CharField(max_length=9, choices=DAY_OF_THE_WEEK)
    start_time = models.TimeField(null=True, max_length=123)
    end_time = models.TimeField(null=True, max_length=123)
    objects = models.Manager()

    def __str__(self):
        return "%s, %s, начало смены: %s; конец смены: %s" % (
        self.dayOfTheWeek, self.name, self.start_time, self.end_time)

    class Meta:
        verbose_name = 'Штатное расписание'
        verbose_name_plural = 'Штатное расписание'
