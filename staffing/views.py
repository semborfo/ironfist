from django.shortcuts import render
from ancillary_stuff.utils import *
from staffing.models import Staffing
from ancillary_stuff.objects import *
import copy


def make_table(request):
    daily_schedule = copy.deepcopy(Staffing.objects.all())
    shifts_placed_by_weekday = make_shifts_by_weekday(daily_schedule)
    shifts_by_weekdays_in_order = dict()
    for weekday in weekdays:
        shifts_by_weekdays_in_order[weekday] = list(
            WorkShift(weekday, item, date=datetime.today()) for item in
            sorted(shifts_placed_by_weekday[weekday], key=operator.attrgetter('start_time')))

    update_persons_with_all_shifts(shifts_by_weekdays_in_order)

    min_time = find_min_time(shifts_by_weekdays_in_order.values())
    max_time = find_max_time(shifts_by_weekdays_in_order.values())

    time_range = (max_time - min_time).seconds // 3600
    TimeRow_time_range = [TimeRow(min_time, i) for i in range(0, time_range + 1)]
    TimeRow_time_range_by_hour = {i.hour: i for i in TimeRow_time_range}
    weekday_inst_by_weekday = {weekday: TR_weekday(weekday) for weekday in weekdays}
    for weekday in shifts_by_weekdays_in_order:
        EmptyShifts = make_empty_shifts(shifts_by_weekdays_in_order, TimeRow_time_range, weekday)
        for person in shifts_by_weekdays_in_order[weekday]:
            TimeRow_time_range_by_hour[person.start_hour].append_shift(person)
        for emp_shift in EmptyShifts:
            TimeRow_time_range_by_hour[emp_shift.start_hour].append_shift(emp_shift)
        colspan = find_colspan(shifts_by_weekdays_in_order[weekday])
        weekday_inst_by_weekday[weekday].set_colspan(colspan)
    return render(request, 'staffing/staffing.html',
                  context={'weekdays': weekday_inst_by_weekday.values(),
                           'timerows': TimeRow_time_range}, )
