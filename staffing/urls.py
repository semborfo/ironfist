from django.urls import path

from staffing import views


# Create your tests here.
urlpatterns = [
    path('', views.make_table, name='show_replacement'),
]